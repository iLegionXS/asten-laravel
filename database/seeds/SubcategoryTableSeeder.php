<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubcategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subcategories')->insert([
            [
                'title_en' => 'on_the_grid',
                'title_ru' => 'На сетку',
                'category_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'perforation',
                'title_ru' => 'На перфорацию',
                'category_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'on_the_chipboard',
                'title_ru' => 'На ДСП',
                'category_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'on_the_economy_panel',
                'title_ru' => 'На эконом панель',
                'category_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'systems',
                'title_ru' => 'Системы крючков',
                'category_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'accessories',
                'title_ru' => 'Аксессуары торговые',
                'category_id' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_trading_nets',
                'title_ru' => 'Сетки торговые',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_for_snack_products',
                'title_ru' => 'Под снековую продукцию',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_under_tights',
                'title_ru' => 'Под колготки',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_under_the_books_newspapers',
                'title_ru' => 'Под книги газеты и журналы',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_under_pos_materials',
                'title_ru' => 'Под POS материалы',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_under_the_seeds',
                'title_ru' => 'Под семена',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_under_the_napkins',
                'title_ru' => 'Под салфетки',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_under_the_thread_and_floss',
                'title_ru' => 'Под нитки и мулине',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_for_beads',
                'title_ru' => 'Для бисера',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_under_the_packages_straps_ties',
                'title_ru' => 'Под пакеты ремни галстуки',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_under_the_paint',
                'title_ru' => 'Под краску',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_under_postcards',
                'title_ru' => 'Под открытки',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_under_the_key_chains_and_jewelry',
                'title_ru' => 'Под брелки и бижутерию',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_perforation',
                'title_ru' => 'Перфорация',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_for_clothes',
                'title_ru' => 'Для одежды',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'room_office_furniture',
                'title_ru' => 'Офисная мебель',
                'category_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'basket_the_rack_is_shopping_basket',
                'title_ru' => 'Стеллаж торговый корзинный',
                'category_id' => 4,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'basket_the_basket_is_floor',
                'title_ru' => 'Корзина напольная',
                'category_id' => 4,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'retail_shelves_on_the_grid',
                'title_ru' => 'На сетку',
                'category_id' => 5,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'furniture_shelving',
                'title_ru' => 'Стеллажи',
                'category_id' => 6,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'furniture_tables',
                'title_ru' => 'Столы',
                'category_id' => 6,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'furniture_little_tables',
                'title_ru' => 'Столики',
                'category_id' => 6,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'furniture_thumbs',
                'title_ru' => 'Тумбы',
                'category_id' => 6,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'furniture_racks_for_clothes',
                'title_ru' => 'Стойки для одежды',
                'category_id' => 6,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title_en' => 'furniture_shelves_mounted',
                'title_ru' => 'Полки навесные',
                'category_id' => 6,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
