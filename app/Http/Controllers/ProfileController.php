<?php

namespace Asten\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = DB::table('categories')->get();
        $subcategories = DB::table('subcategories')->get();

        return view('profile.index', [
            'categories' => $categories,
            'subcategories' => $subcategories,
        ]);
    }

    /** Сохранение Ф.И.О, телефона и города
     * TODO: добавить возможность сохранения только одного из полей.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveUserData(Request $request)
    {
        $request->fullName = explode(' ', $request->fullName);

        $validate = Validator::make($request, [
            'fullName.0' => 'max:255|string',
            'fullName.1' => 'max:255|string',
            'fullName.2' => 'max:255|string',
            'phone' => 'digits:10'
        ]);

        DB::table('users')->insert([
            'surname' => $request->fullName[0],
            'name' => $request->fullName[1],
            'middlename' => $request->fullName[2],
            'phone' => $request->phone,
            'city' => $request->city,
        ]);

        return response()->json($validate);
    }
}
