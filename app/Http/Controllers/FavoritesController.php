<?php

namespace Asten\Http\Controllers;

use Asten\Model\Goods;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FavoritesController extends Controller
{
    public function index()
    {
        $categories = DB::table('categories')->get();
        $subcategories = DB::table('subcategories')->get();

        return view('favorites', [
            'categories' => $categories,
            'subcategories' => $subcategories,
        ]);
    }

    public function getFavorites(Request $request)
    {
        $goods_obj = new Goods();
        $goods_obj->id = $request->id;
        $goods_obj->category = $request->category;
        $goods_obj->subcategory = $request->subcategory;

        return $this->showCardFavorites($goods_obj->getByID());
    }

    public function showCardFavorites($goods)
    {
        return view('layouts.card-favorites', ['goods_item' => $goods]);
    }
}
