<?php

namespace Asten\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactsController extends Controller
{
    public function index()
    {
        $categories = DB::table('categories')->get();
        $subcategories = DB::table('subcategories')->get();

        return view('contacts', ['categories' => $categories, 'subcategories' => $subcategories]);
    }
}
