<?php

namespace Asten\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SaleController extends Controller
{
    public function index()
    {
        $categories = DB::table('categories')->get();
        $subcategories = DB::table('subcategories')->get();

        return view('sale', [
            'categories' => $categories,
            'subcategories' => $subcategories,
        ]);
    }
}
