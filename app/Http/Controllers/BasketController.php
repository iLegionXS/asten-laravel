<?php

namespace Asten\Http\Controllers;

use Asten\Model\Basket;
use Barryvdh\DomPDF\Facade as PDF;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BasketController extends Controller
{
    private $basket_obj;

    public function __construct()
    {
        $this->basket_obj = new Basket();
    }

    public function index(Request $request)
    {
        $categories = DB::table('categories')->get();
        $subcategories = DB::table('subcategories')->get();
        $item = $this->basket_obj->getItem();

        if ($request->vat) $item = $this->basket_obj->getItem(1);

//        dump(Cart::restore(1));

        return view('basket', [
            'categories' => $categories,
            'subcategories' => $subcategories,
            'cart' => $item,
        ]);
    }

    public function addItem(Request $request)
    {
        $this->basket_obj->addItem($request);
        return redirect('/basket');
    }

    public function delItem(Request $request)
    {
        $this->basket_obj->delItem($request->del);
        return redirect('/basket');
    }

    public function getLastCartID()
    {
        return DB::table('shoppingcart')->orderBy('created_at', 'desc')->first();
    }


    /** Сохранение корзины в бд.
     * TODO: реализовать до конца для авторизированого пользователя.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveItem(Request $request)
    {
        $cart_id = $this->getLastCartID();
//        Cart::store(1);

        return response()->json(Cart::content());
    }
}
