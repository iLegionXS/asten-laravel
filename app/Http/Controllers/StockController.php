<?php

namespace Asten\Http\Controllers;

use Asten\Model\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockController extends Controller
{
    public function index()
    {
        $categories = DB::table('categories')->get();
        $subcategories = DB::table('subcategories')->get();
        $stock_obj = new Stock();
        $goods = [];

        foreach ($stock_obj->get() as $item) {
            $goods[] = DB::table($item->subcategory)->where('id', $item->goods_id)->first();
        }

        return view('stock', [
            'categories' => $categories,
            'subcategories' => $subcategories,
            'goods' => $goods,
            'stock' => $stock_obj->get(),
        ]);
    }
}
