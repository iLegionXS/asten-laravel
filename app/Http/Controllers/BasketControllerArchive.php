<?php

namespace Asten\Http\Controllers;

use Asten\Model\Basket;
use Asten\Model\Goods;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BasketControllerArchive extends Controller
{
    public function index()
    {
        $categories = DB::table('categories')->get();
        $subcategories = DB::table('subcategories')->get();

        return view('basket', ['categories' => $categories, 'subcategories' => $subcategories]);
    }

    public function getBasket(Request $request)
    {
        $goods_obj = new Goods();
        $basket_obj = new Basket();

        $goods_obj->id = $request->id;
        $goods_obj->category = $request->category;
        $goods_obj->subcategory = $request->subcategory;

        $goods = $goods_obj->getByID();
        $goods->color = $request->color;
        $goods->count = $request->count;
        $goods->vat = $request->vat;
        $goods->total = $basket_obj->sum($goods);

        if ($request->vat) $goods->total = $basket_obj->sum($goods, 1);

        if (!$request->session()->has('cart-total')) {
            $request->session()->push('cart-total', 0);
        } else {
            $request->session()->push('cart-total', $goods->total);
        }

//        $total_price = $request->session()->get('cart-total');
//        $total_price[0] += $goods->total;
//

//        $request->session()->flush();

        return $this->showCardBasket($goods);
    }

    public function showCardBasket($goods)
    {
        return view('layouts.card-basket', ['goods_item' => $goods]);
    }

    public function getTotalPrice(Request $request)
    {
        return response()->json($request->session()->all());
    }
}
