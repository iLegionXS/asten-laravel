<?php

namespace Asten\Http\Controllers;

use Asten\Model\Goods;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index()
    {
//        Auth::logout();
        $categories = DB::table('categories')->get();
        $subcategories = DB::table('subcategories')->get();
        $sliders = DB::table('slider')->get();
        $sales_leaders = DB::table('sales_leaders')->get();
        $goods = [];

        foreach ($sales_leaders as $item) {
            $goods[] = DB::table($item->subcategory)->where('id', $item->goods_id)->first();
        }

        return view('index', [
            'categories' => $categories,
            'subcategories' => $subcategories,
            'sliders' => $sliders,
            'sales_leaders_goods' => $goods,
        ]);
    }

    /** Получение данных для "Недавно просмотренных"
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function recentlyViewed(Request $request)
    {
        $goods_obj = new Goods();
        $goods_obj->id = $request->id;
        $goods_obj->category = $request->category;
        $goods_obj->subcategory = $request->subcategory;

        return $this->showCardStandard($goods_obj->getByID());
    }

    public function showCardStandard($rvgoods)
    {
        return view('layouts.card-standard', ['goods_item' => $rvgoods]);
    }
}
