<?php

namespace Asten\Http\Controllers;

use Asten\Model\Category;
use Asten\Model\Stock;
use Asten\Model\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{
    public function index($category_id, $subcategory_id = null, $goods_id = null)
    {
        $categories = DB::table('categories')->get();
        $subcategories = DB::table('subcategories')->get();

        foreach ($categories as $item) {
            if ($item->id == $category_id) {
                $category_select = $item;
            }
        }

        foreach ($subcategories as $item) {
            if ($item->id == $subcategory_id) {
                $subcategory_select = $item;
            }
        }

        if ($goods_id) {
            $goods_item = DB::table($subcategory_select->title_en)->where('id', $goods_id)->first();
            $stock_item = DB::table('stock')->select('price_fop', 'price_nds', 'count', 'color')->where('goods_id', $goods_id)->first();

            return view('details', [
                'categories' => $categories,
                'subcategories' => $subcategories,
                'category_select' => $category_select,
                'subcategory_select' => $subcategory_select,
                'goods_item' => $goods_item,
                'stock' => $stock_item,
            ]);
        }

        if ($subcategory_id) {
            $goods = DB::table($subcategory_select->title_en)->get();
            $stock_obj = new Stock();
            $subcategory_obj = new Subcategory();
            $stock_goods = [];

            foreach ($goods as $item) {
                $stock_obj->id = $item->id;
                $subcategory_obj->id = $item->subcategory;
                $subcategory = $subcategory_obj->getByID();
                $stock_obj->subcategory = $subcategory->title_en;
                $item->stock = $stock_obj->getByID();
                $stock_goods[] = $stock_obj->getByID();
            }

            return view('goods', [
                'categories' => $categories,
                'subcategories' => $subcategories,
                'category_select' => $category_select,
                'subcategory_select' => $subcategory_select,
                'goods' => $goods,
                'stock' => $stock_goods,
            ]);
        }

        return view('subcategories', [
            'categories' => $categories,
            'subcategories' => $subcategories,
            'category_select' => $category_select,
        ]);
    }
}
