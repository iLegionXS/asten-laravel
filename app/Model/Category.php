<?php

namespace Asten\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    public $id;
    protected $table = "categories";

    public function getByID()
    {
        return DB::table($this->table)->where('id', $this->id)->first();
    }
}
