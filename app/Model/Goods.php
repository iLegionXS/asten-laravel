<?php

namespace Asten\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Goods extends Model
{
    public $id;
    public $category;
    public $subcategory;

    public function getByID()
    {
        $subcategory_obj = new Subcategory();
        $subcategory_obj->id = $this->subcategory;
        $subcategory = $subcategory_obj->getByID();

        return DB::table($subcategory->title_en)->where('id', $this->id)->first();
    }
}
