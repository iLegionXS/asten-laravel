<?php

namespace Asten\Model;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Basket
 * If you got here, then things are bad. Here is a very confusing code,
 * wrote quickly, so good luck to you.
 * @package Asten\Model
 */
class Basket extends Model
{
    private $goods_obj;
    private $category_obj;

    public function __construct(array $attributes = [])
    {
        $this->goods_obj = new Goods();
        $this->category_obj = new Category();
    }

    public function addItem($item)
    {
        Cart::add([
            'id' => $item->id,
            'name' => $this->getGoods($item->id, $item->subcategory)->title,
            'qty' => (int)$item->count,
            'price' => 0,
            'options' => [
                'subcategory' => $item->subcategory,
                'color' => $item->color,
            ],
        ]);
    }

    public function getItem($vat = null)
    {
        $cart = Cart::content();
        $sum = 0;

        if (Cart::count() === 0) {
            return null;
        }

        foreach ($cart as $item) {
            $this->goods_obj->id = $item->id;
            $this->goods_obj->subcategory = $item->options->subcategory;
            $item->price = $this->calc($item);

            if ($vat) $item->price = $this->calc($item, 1);

            $item->goods = $this->goods_obj->getByID();
            $sum += $item->price;
        }

        $cart->sum = $sum;
        return $cart;
    }

    public function delItem($rowId)
    {
        Cart::remove($rowId);
    }

    public function saveItem()
    {
        Cart::store(1);
    }

    private function getGoods($id, $subcategory)
    {
        $this->goods_obj->id = $id;
        $this->goods_obj->subcategory = $subcategory;
        return $this->goods_obj->getByID();
    }

    private function calc($goods, $vat = null)
    {
        $goodsItem = $this->getGoods($goods->id, $goods->options->subcategory);
        $this->category_obj->id = $goodsItem->category;
        $categoryTitleEN = $this->category_obj->getByID()->title_en;

        if ($categoryTitleEN === 'hook') {
            $c = $this->calcForHooks($goods->options->color, $goods->qty, $goodsItem->price_one, $goodsItem->price_two, $vat);
        } else {
            $c = $this->calcForOther($goods->options->color, $goodsItem->price_one, $goodsItem->price_two, $goodsItem->price_three, $goods->qty, $goodsItem->optimal_count, $vat);
        }

        return $c;
    }

    private function calcForHooks($color, $count, $price_one, $price_two, $vat = null)
    {
        $coeff = $this->coeffForHooks($color);

        if ($count < 1000) {
            $total = $this->calcUpToThousand($price_one, $coeff, $count);

            if ($vat) {
                $total *= 1.2;
            }
        } else {
            $total = $this->calcMoreThousand($price_two, $coeff, $count);

            if ($vat) {
                $val = $price_two;
                $val *= $coeff;
                $s = 0.05;
                $total = ceil($val / $s) * $s;
                $total *= 1.06;
                $total /= 6;
                $total = round($total, 3, PHP_ROUND_HALF_UP);
                $total = round($total, 2, PHP_ROUND_HALF_UP);
                $total *= 6;
                $total *= $count;
            }
        }

        return $total;
    }

    private function calcForOther($color, $price_one, $price_two, $price_three, $count, $optimal_count, $vat = null)
    {
        $coeff = $this->coeffForOther($color, $price_one);

        if ($count < $optimal_count) {
            $total = $this->calcUpToThousandOther($count, $price_one, $coeff);

            if ($vat) $total *= 1.2;

        } else {
            $total = $this->calcMoreThousandOther($count, $price_two, $coeff);

            if ($vat) {
                if ($color == 9003) {
                    $val = $price_three;
                } else {
                    $val = $price_two;
                    $val *= $coeff;
                    $s = 5;
                    $val = ceil($val / $s) * $s;
                    $val *= 1.15;
                    $val /= 6;
                    $val = ceil($val / 0.5) * 0.5;
                    $val *= 6;
                }

                $val *= $count;
                $total = $val;
            }
        }

        return $total;
    }

    private function coeffForHooks($color)
    {
        switch ($color) {
            case '9003':
                $multiplication = 1;
                break;

            case '7657':
                $multiplication = 1.09;
                break;

            default:
                $multiplication = 1.04;
                break;
        }

        return $multiplication;
    }

    private function coeffForOther($color, $price)
    {
        switch ($color) {
            case '9003':
                $multiplication = 1;
                break;

            case '7657':
                if ($price <= 1000) {
                    $multiplication = 1.25;
                } elseif ($price <= 3000) {
                    $multiplication = 1.15;
                } else {
                    $multiplication = 1.1;
                }
                break;

            default:
                if ($price <= 1000) {
                    $multiplication = 1.2;
                } elseif ($price <= 3000) {
                    $multiplication = 1.1;
                } else {
                    $multiplication = 1.07;
                }
                break;
        }

        return $multiplication;
    }

    private function calcUpToThousand($price_one, $coeff, $count)
    {
        $val = $price_one;
        $val *= $coeff;
        $s = 0.05;
        $val = ceil($val / $s) * $s;
        $val *= $count;

        return $val;
    }

    private function calcMoreThousand($price_two, $coeff, $count)
    {
        $val = $price_two;
        $val *= $coeff;
        $s = 0.05;
        $val = ceil($val / $s) * $s;
        $val *= $count;

        return $val;
    }

    private function calcUpToThousandOther($count, $price_one, $coeff)
    {
        $val = $price_one;
        $val *= $coeff;
        $s = 5;
        $val = ceil($val / $s) * $s;
        $val *= $count;

        return $val;
    }

    private function calcMoreThousandOther($count, $price_two, $coeff)
    {
        $val = $price_two;
        $val *= $coeff;
        $s = 5;
        $val = ceil($val / $s) * $s;
        $val *= $count;

        return $val;
    }
}
