<?php

namespace Asten\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Stock extends Model
{
    protected $table = 'stock';
    public $id;
    public $category_id;
    public $subcategory;

    public function get()
    {
        return DB::table($this->table)->get();
    }

    public function getByID()
    {
        return DB::table($this->table)->where('goods_id', $this->id)->where('subcategory', $this->subcategory)->first();
    }
}
