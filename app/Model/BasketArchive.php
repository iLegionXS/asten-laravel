<?php

namespace Asten\Model;

use Illuminate\Database\Eloquent\Model;

class BasketArchive extends Model
{
    public function sum($goods, $vat = null)
    {
        $category_obj = new Category();
        $category_obj->id = $goods->category;

        if ($category_obj->getByID()->title_en === 'hook') {
            $coeff = $this->coeffForHooks($goods->color);

            if ($goods->count < 1000) {
                $total = $this->calcUpToThousand($goods->count, $goods->price_one, $coeff);

                if ($vat) $total *= 1.2;

            } else {
                $total = $this->calcMoreThousand($goods->count, $goods->price_two, $coeff);

                if ($vat) {
                    $val = $goods->price_two;
                    $val *= $coeff;
                    $s = 0.05;
                    $total = ceil($val / $s) * $s;
                    $total *= 1.06;
                    $total /= 6;
                    $total = round($total, 3, PHP_ROUND_HALF_UP);
                    $total = round($total, 2, PHP_ROUND_HALF_UP);
                    $total *= 6;
                    $total *= $goods->count;
                }
            }
        } else {
            $coeff = $this->coeffForOther($goods->color, $goods->price_one);

            if ($goods->count < $goods->optimal_count) {
                $total = $this->calcUpToThousandOther($goods->count, $goods->price_one, $coeff);

                if ($vat) $total *= 1.2;

            } else {
                $total = $this->calcMoreThousandOther($goods->count, $goods->price_two, $coeff);

                if ($vat) {
                    if ($goods->color == 9003) {
                        $val = $goods->price_three;
                    } else {
                        $val = $goods->price_two;
                        $val *= $coeff;
                        $s = 5;
                        $val = ceil($val / $s) * $s;
                        $val *= 1.15;
                        $val /= 6;
                        $val = ceil($val / 0.5) * 0.5;
                        $val *= 6;
                    }

                    $val *= $goods->count;
                    $total = $val;
                }
            }
        }

        return $total;
    }

    private function coeffForHooks($color)
    {
        switch ($color) {
            case '9003':
                $multiplication = 1;
                break;

            case '7657':
                $multiplication = 1.09;
                break;

            default:
                $multiplication = 1.04;
                break;
        }

        return $multiplication;
    }

    private function coeffForOther($color, $price)
    {
        switch ($color) {
            case '9003':
                $multiplication = 1;
                break;

            case '7657':
                if ($price <= 1000) {
                    $multiplication = 1.25;
                } elseif ($price <= 3000) {
                    $multiplication = 1.15;
                } else {
                    $multiplication = 1.1;
                }
                break;

            default:
                if ($price <= 1000) {
                    $multiplication = 1.2;
                } elseif ($price <= 3000) {
                    $multiplication = 1.1;
                } else {
                    $multiplication = 1.07;
                }
                break;
        }

        return $multiplication;
    }

    private function calcUpToThousand($count, $price_one, $coeff)
    {
        $val = $price_one;
        $val *= $coeff;
        $s = 0.05;
        $val = ceil($val / $s) * $s;
        $val *= $count;

        return $val;
    }

    private function calcMoreThousand($count, $price_two, $coeff)
    {
        $val = $price_two;
        $val *= $coeff;
        $s = 0.05;
        $val = ceil($val / $s) * $s;
        $val *= $count;

        return $val;
    }

    private function calcUpToThousandOther($count, $price_one, $coeff)
    {
        $val = $price_one;
        $val *= $coeff;
        $s = 5;
        $val = ceil($val / $s) * $s;
        $val *= $count;

        return $val;
    }

    private function calcMoreThousandOther($count, $price_two, $coeff)
    {
        $val = $price_two;
        $val *= $coeff;
        $s = 5;
        $val = ceil($val / $s) * $s;
        $val *= $count;

        return $val;
    }
}
