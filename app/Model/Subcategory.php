<?php

namespace Asten\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Subcategory extends Model
{
    public $id;
    protected $table = 'subcategories';

    public function getByID()
    {
        return DB::table($this->table)->where('id', $this->id)->first();
    }
}
