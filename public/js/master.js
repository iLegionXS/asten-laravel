'use strict';

function initCarousel() {
    $('.carousel').slick({
        dots: false,
        slidesToShow: 4,
        prevArrow: '<i class="carousel-prev asten_icon-arrow_prev" aria-label="Previous"></i>',
        nextArrow: '<i class="carousel-next asten_icon-arrow_next_2" aria-label="Next"></i>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    variableWidth: false,
                },
            },
            {
                breakpoint: 587,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                },
            },
        ],
    });
}

$(document).ready(function () {
    $('.slider').slick({
        lazyLoad: 'ondemand',
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                },
            },
        ],
    });

    $('.slick-vertical').slick({
        lazyLoad: 'ondemand',
        dots: false,
        vertical: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: '<i class="carousel-prev asten_icon-arrow_prev" aria-label="Previous"></i>',
        nextArrow: '<i class="carousel-next asten_icon-arrow_next_2" aria-label="Next"></i>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    vertical: false,
                }
            }
        ]
    })

    initCarousel();
});

function changeImgInDetails(path) {
    $('.img-main img').attr('src', path);
}

function changeDetailsTabs(elem) {
    let parentTabs = document.getElementsByClassName('head-details-head')[0];
    let active = parentTabs.getElementsByClassName('active');
    let activeAttr = active[0].getAttribute('data-value');
    let activeDiv = document.getElementById(activeAttr);

    active[0].classList.remove('active');
    activeDiv.classList.add('d-none');
    elem.classList.add('active');
    let elemAttr = elem.getAttribute('data-value');
    document.getElementById(elemAttr).classList.remove('d-none');
}

function addFavorites(id, category, subcategory) {
    let data = [id, category, subcategory];

    function checkEmpty() {
        return localStorage.getItem('favorites');
    }

    function checkSimilar() {
        let error;
        let ls_data = JSON.parse(localStorage.getItem('favorites'));

        for (let index = 0; index < 10; index++) {
            if (JSON.stringify(ls_data[index]) === JSON.stringify(data)) {
                return error = 1;
            }
        }
    }

    function save() {
        let arr = [data];

        if (checkEmpty() === null) {
            localStorage.setItem('favorites', JSON.stringify(arr))
        } else if (checkSimilar() === undefined) {
            let dataFromLS = JSON.parse(localStorage.getItem('favorites'));
            dataFromLS.push(data);
            localStorage.setItem('favorites', JSON.stringify(dataFromLS));
        }

        let dataFromLS = JSON.parse(localStorage.getItem('favorites'));
        let count = dataFromLS.length;
        if (count > 9) {
            let mod_ls_data = dataFromLS.slice(-10);
            localStorage.setItem('favorites', JSON.stringify(mod_ls_data));
        }
    }

    save();
}

function deleteGoodsFromFavorites(elem, id, subcategory) {
    let card = elem.parentElement.parentElement.parentElement;
    let favorites_ls = JSON.parse(localStorage.getItem('favorites'));

    favorites_ls.forEach(function (item, i, favorites_ls) {
        if (item[0] === id && item[2] === subcategory) {
            favorites_ls.splice(i, 1);
        }
    });

    card.remove();
    localStorage.setItem('favorites', JSON.stringify(favorites_ls));

    if (favorites_ls) {
        localStorage.removeItem('favorites');
    }

    window.location.reload();
}

$('.modal').iziModal();

function openModalAuth() {
    $('#modalAuth').iziModal('open');
}

function closeModalAuth() {
    $('#modalAuth').iziModal('close');
}

function changeAuthTab(elem) {
    if ($(elem).text() === 'Регистрация') {
        $('.login').fadeOut(200, function () {
            $('.register').fadeIn();
        });
        $('.modalAuth-header span').text('Регистрация');
        $(elem).text('Вход');
    } else if ($(elem).text() === 'Вход') {
        $('.register').fadeOut(200, function () {
            $('.login').fadeIn();
        });
        $('.modalAuth-header span').text('Вход');
        $(elem).text('Регистрация');
    } else {
        $('.login').fadeOut(200, function () {
            $('.register').fadeOut(200, function () {
                $('.reset').fadeIn();
            });
        });
        $('.modalAuth-header span').text('Сброс пароля');
        $('.modalAuth-header a').remove();
    }
}

function fadeToggle(elem) {
    $(elem).fadeToggle(200);
}
