@extends('layouts.app')

@section('content')

    <div class="breadcrumbs-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="/"><i class="asten_icon-home"></i></a> <span>/</span> Акции
                </div>
            </div>
        </div>
    </div>

    <div class="stock-head-block head-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="stock-head head-block-head">
                        <h1>Акции</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="stock-content-block head-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="stock-content-head">
                        <p>
                            В данном разделе Вы можете преобрести наши акционные товары. Акции дляться некоторое время,
                            так что поспешите.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="stock-content-card">
                        @foreach($goods as $goods_item)
                            @include('layouts.card-stock')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
