@extends('layouts.app')

@section('content')

    <div class="breadcrumbs-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="/"><i class="asten_icon-home"></i></a> <span>/</span> Распродажа
                </div>
            </div>
        </div>
    </div>

    <div class="sale-head-block head-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="sale-head head-block-head">
                        <h1>РАСПРОДАЖА</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sale-content-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="sale-content-head">
                        <p>
                            В данном разделе Вы можете преобрести наши товары какие-то слова просто для заполнения пустоты.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="sale-content-card">

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection