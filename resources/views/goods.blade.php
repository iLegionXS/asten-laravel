@extends('layouts.app')

@section('content')

    <div class="breadcrumbs-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="/">
                        <i class="asten_icon-home"></i>
                    </a>
                    <span>/</span>
                    <a href="/shop/{{ $category_select->id }}">{{ $category_select->title_ru }}</a>
                    <span>/</span>
                    {{ $subcategory_select->title_ru }}
                </div>
            </div>
        </div>
    </div>

    <div class="goods-head-block head-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="head-block-head">
                        <h1>{{ $subcategory_select->title_ru }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="goods-content-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="goods-content-card">
                        @foreach($goods as $goods_item)
                            @if($goods_item->stock === null)
                                @include('layouts.card-standard')
                            @else
                                @include('layouts.card-stock')
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

