@extends('layouts.app')

@section('content')

    <div class="slider_block">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-4"></div>
                <div class="col-xl-9 col-lg-8 col-md-12">
                    <div class="slider">
                        @foreach($sliders as $slider)
                            <div class="slider-item">
                                <img src="/images/uploaded/slider/{{ $slider->img }}"
                                     data-lazy="/images/uploaded/slider/{{ $slider->img }}" alt="{{ $slider->head }}">
                                <div class="slider-content">
                                    <div class="slider-content-head">{{ $slider->head }}</div>
                                    <p class="slider-content-text">
                                        {{ $slider->description }}
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sales_leaders_block">
        <div class="sales_leaders_head head-title">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2><i class="asten_icon-leader"></i> Лидеры <span>продаж</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="sales_leaders_content">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="carousel">
                            @foreach($sales_leaders_goods as $goods_item)
                                @include('layouts.card-standard')
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="viewed_products_block">
        <div class="viewed_products_head head-title">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2><i class="asten_icon-eye"></i> Просмотренные <span>товары</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="viewed_products_content">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="viewed_products_carousel carousel"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#nav_btn .catalog').css('display', 'block');
        let recentlyViewedData = localStorage.getItem('recently-viewed');

        function getRVD(id, category, subcategory) {
            let data = {
                'id': id,
                'category': category,
                'subcategory': subcategory,
            };

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "/recently_viewed",
                method: 'GET',
                data: data,
                success: function (response) {
                    $('.viewed_products_block .viewed_products_carousel').slick('unslick');
                    $('.viewed_products_block .viewed_products_carousel').append(response);
                    $('.viewed_products_block .viewed_products_carousel').slick({
                        dots: false,
                        slidesToShow: 4,
                        prevArrow: '<i class="carousel-prev asten_icon-arrow_prev" aria-label="Previous"></i>',
                        nextArrow: '<i class="carousel-next asten_icon-arrow_next_2" aria-label="Next"></i>',
                        responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 3,
                                },
                            },
                            {
                                breakpoint: 992,
                                settings: {
                                    slidesToShow: 2,
                                    variableWidth: false,
                                },
                            },
                            {
                                breakpoint: 587,
                                settings: {
                                    slidesToShow: 1,
                                    arrows: false,
                                },
                            },
                        ],
                    });
                }
            });
        }

        if (recentlyViewedData !== null) {
            let parsedRVD = JSON.parse(recentlyViewedData);

            for (let i = 0; i < parsedRVD.length; i++) {
                getRVD(parsedRVD[i][0], parsedRVD[i][1], parsedRVD[i][2]);
            }

            $('.viewed_products_block').css('display', 'block');
        }
    </script>
@endsection