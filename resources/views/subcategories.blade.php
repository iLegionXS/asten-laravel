@extends('layouts.app')

@section('content')

    <div class="breadcrumbs-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="/">
                        <i class="asten_icon-home"></i>
                    </a>
                    <span>/</span>
                    {{ $category_select->title_ru }}
                </div>
            </div>
        </div>
    </div>

    <div class="head-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="head-block-head">
                        <h1>{{ $category_select->title_ru }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="subcategories-content-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="subcategories-content-list">
                        @foreach($subcategories as $subcategory)
                            @if($subcategory->category_id === $category_select->id)
                                <div>
                                    <a href="/shop/{{ $category_select->id }}/{{ $subcategory->id }}">
                                        {{ $subcategory->title_ru }}
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection