<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="apple-touch-icon" href="/apple-touch-favicon.png">


    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto:300,400,400i,500,700|Rubik:700"
          rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/slick/slick.css">
    <link rel="stylesheet" href="/css/slick/slick-theme.css">
    <link rel="stylesheet" href="/css/asten_icons/asten_icons.css">
    <link rel="stylesheet" href="/css/iziModal.min.css">
    <link rel="stylesheet" href="/scss/style.css">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script src="/js/jquery-3.4.0.min.js"></script>
</head>
<body>
<header id="header">
    <div class="head">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <div class="logo">
                        <a href="/"><img src="/images/nav/asten_logo_horiz.svg" alt="Логотип"></a>
                    </div>
                </div>
                <div class="col-9">
                    <div class="links_to_our_sites">
                        <a href="#">Производитель</a>
                        <a href="#">Порошковая покраска</a>
                        <a href="#">Услуги металлообработки</a>
                    </div>
                    <div class="search_and_icons">
                        <div class="search">
                            <form action="">
                                <input type="text" placeholder="Найти">
                                <button type="submit"><i class="asten_icon-search"></i></button>
                            </form>
                        </div>
                        <div class="icons">
                            <a href="/favorites"><i class="asten_icon-favorite"></i></a>
                            @if(auth()->user())
                                <a href="/profile"><i class="asten_icon-user"></i></a>
                            @else:
                            <a href="#" onclick="openModalAuth()"><i class="asten_icon-user"></i></a>
                            @endif

                            <a href="/basket"><i class="asten_icon-shopping_cart"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="nav">
        <div class="container">
            <ul class="menu">
                <li id="nav_btn">
                    <a href="#" class="catalog_btn">
                        <i class="asten_icon-burger"></i>
                        Каталог <span>продукции</span>
                    </a>
                    <ul class="catalog">
                        <li class="catalog-empty"></li>
                        @foreach($categories as $category)
                            <li>
                                <a href="/shop/{{ $category->id }}">
                                    <span>{{ $category->title_ru }}</span>
                                    <i class="asten_icon-arrow_next"></i>
                                </a>
                                <div class="subcategory">
                                    <ul class="subcategory_list">
                                        @foreach($subcategories as $subcategory)
                                            @if($subcategory->category_id === $category->id)
                                                <li>
                                                    <a href="/shop/{{ $category->id }}/{{ $subcategory->id }}">
                                                        {{ $subcategory->title_ru }}
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li><a href="#">О нас</a></li>
                <li><a href="/stock">Акции</a></li>
                <li><a href="/sale">Распродажа</a></li>
                <li><a href="/payment">Оплата и доставка</a></li>
                <li><a href="/contacts">Контакты</a></li>
            </ul>
        </div>
    </nav>
</header>
<div id="header_mobile">
    <div class="head_mobile">
        <div class="container-fluid">
            <div class="row">
                <div class="col d-flex align-items-center justify-content-between justify-content-sm-center">
                    <div class="logo">
                        <a href="/"><img src="/images/nav/asten_logo_horiz.svg" alt="Логотип"></a>
                        <div class="nav_mobile_brand"><a href="/">ASTEN</a></div>
                    </div>
                    <div class="mobile_icons">
                        <a href="#"><i class="asten_icon-search"></i></a>
                        <a href="/favorites"><i class="asten_icon-favorite"></i></a>
                        <?php if (!isset($_SESSION['id'])): ?>
                        <a href="/profile"><i class="asten_icon-user"></i></a>
                        <?php else: ?>
                        <a href="#" onclick="openModalAuth()"><i class="asten_icon-user"></i></a>
                        <?php endif ?>
                        <a href="/basket"><i class="asten_icon-shopping_cart"></i></a>
                    </div>
                    <div class="search">
                        <form action="">
                            <input type="text" placeholder="Найти">
                            <button type="submit"><i class="asten_icon-search"></i></button>
                        </form>
                    </div>
                    <div class="collapse_mobile">
                        <button type="button" onclick="openCollapseMenu()">
                            <img src="/images/nav/burger.png" alt="Открыть меню">
                        </button>
                        <div id="collapse_nav">
                            <div class="collapse_nav_head">
                                <div>Меню</div>
                                <button type="button" onclick="closeCollapseMenu()"><img src="/images/nav/cross.png"
                                                                                         alt="Закрыть меню">
                                </button>
                            </div>
                            <div class="collapse_nav_content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <div class="collapse_nav_content_btn d-flex flex-column justify-content-start">
                                                <button type="button" onclick="openCloseSubmenu()">
                                                    <span>Каталог</span> продукции <i
                                                            class="asten_icon-arrow_next"></i></button>
                                                <ul class="collapse_nav_submenu">
                                                    @foreach($categories as $category)
                                                        <li>
                                                            <a href="/">{{ $category->title_ru }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="collapse_nav_icons d-flex flex-column justify-content-around">
                                                <a href="#"><i class="asten_icon-user"></i> Личный кабинет</a>
                                                <a href="/basket"><i class="asten_icon-shopping_cart"></i> Корзина</a>
                                                <a href="/favorites"><i class="asten_icon-favorite"></i> Избранное</a>
                                            </div>
                                            <div class="collapse_nav_menu d-flex flex-column justify-content-around">
                                                <a href="#">О нас</a>
                                                <a href="/stock">Акции</a>
                                                <a href="/sale">Распродажа</a>
                                                <a href="/payment">Оплата и доставка</a>
                                                <a href="/contacts">Контакты</a>
                                            </div>
                                            <div class="collapse_nav_our_sites d-flex flex-column justify-content-around">
                                                <a href="#">Производитель</a>
                                                <a href="#">Порошковая покраска</a>
                                                <a href="#">Услуги металлообработки</a>
                                            </div>
                                            <div class="collapse_social_links d-flex flex-row justify-content-around">
                                                <a href="#"><i class="asten_icon-facebook"></i></a>
                                                <a href="#"><i class="asten_icon-twitter"></i></a>
                                                <a href="#"><i class="asten_icon-whatsapp"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content">
    @yield('content')
</div>

<div id="footer">
    <div class="footer_info">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-2 d-flex align-items-center">
                    <img src="/images/asten_logo_vert.png" alt="Логотип">
                </div>
                <div class="footer-align col-3">
                    <h3>Контакты</h3>
                    <ul class="contacts">
                        <li><i class="asten_icon-marker"></i>г. Полтава, ул. Комарова 5</li>
                        <li><i class="asten_icon-email"></i>tovvybor97@gmail.com</li>
                        <li><i class="asten_icon-phone_in_talk"></i>+38 (099) 55-16-001</li>
                        <li><i class="asten_icon-phone_in_talk"></i>+38 (096) 04-21-777</li>
                        <li><i class="asten_icon-phone_in_talk"></i>+38 (063) 60-75-307</li>
                        <li class="social_links">
                            <i class="asten_icon-facebook"></i>
                            <i class="asten_icon-twitter"></i>
                            <i class="asten_icon-whatsapp"></i>
                        </li>
                    </ul>
                </div>
                <div class="footer-align col-3">
                    <h3>Каталог продукции</h3>
                    <ul>
                        @foreach($categories as $category)
                            <li>
                                <a href="/shop/">{{ $category->title_ru }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="footer-align col offset-1">
                    <h3>Информация</h3>
                    <ul>
                        <li><a href="#">О нас</a></li>
                        <li><a href="/stock">Акции</a></li>
                        <li><a href="/sale">Распродажа</a></li>
                        <li><a href="/payment">Оплата и доставка</a></li>
                        <li><a href="/contacts">Контакты</a></li>
                        <li><a href="#">Условия использования сайта</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_asten">
        <div class="container">
            <div class="row justify-content-center align-items-center">ASTEN 2019</div>
        </div>
    </div>
</div>
<div id="footer_mobile">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6 border-r border-b">
                <div class="logo">
                    <img src="/images/asten_logo_horiz.png" alt="Логотип">
                    <ul>
                        <li>
                            <a href="#" onclick="openModalAuth()"><i class="asten_icon-user"></i> Личный кабинет</a>
                        </li>
                        <li><a href="/basket"><i class="asten_icon-shopping_cart"></i> Корзина</a></li>
                        <li><a href="/favorites"><i class="asten_icon-favorite"></i> Избранное</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-6 border-b d-flex flex-column align-items-center">
                <div class="footer_mobile_wrap">
                    <h3>Контакты</h3>
                    <ul class="contacts">
                        <li><i class="asten_icon-marker"></i>г. Полтава, ул. Комарова 5</li>
                        <li><i class="asten_icon-email"></i>tovvybor97@gmail.com</li>
                        <li><i class="asten_icon-phone_in_talk"></i>+38 (099) 55-16-001</li>
                        <li><i class="asten_icon-phone_in_talk"></i>+38 (096) 04-21-777</li>
                        <li><i class="asten_icon-phone_in_talk"></i>+38 (063) 60-75-307</li>
                        <li class="social_links">
                            <i class="asten_icon-facebook"></i>
                            <i class="asten_icon-twitter"></i>
                            <i class="asten_icon-whatsapp"></i>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6 border-r d-flex flex-column align-items-center">
                <div class="footer_mobile_wrap">
                    <h3 class="footer_last_head">Каталог</h3>
                    <ul class="footer_mobile_nav">
                        @foreach($categories as $category)
                            <li>
                                <a href="/">{{ $category->title_ru }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-6 d-flex flex-column align-items-center">
                <div class="footer_mobile_wrap">
                    <h3 class="footer_last_head">Информация</h3>
                    <ul class="footer_mobile_info">
                        <li><a href="#">О нас</a></li>
                        <li><a href="/stock">Акции</a></li>
                        <li><a href="/sale">Распродажа</a></li>
                        <li><a href="/payment">Оплата и доставка</a></li>
                        <li><a href="/contacts">Контакты</a></li>
                        <li><a href="#">Условия использования сайта</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_asten">
        <div class="container">
            <div class="row justify-content-center align-items-center">ASTEN 2019</div>
        </div>
    </div>
</div>

<div id="modalAuth" class="modal">
    <div class="modalAuthWrap">
        <div class="modalAuth-header">
            <span>Вход на сайт</span>
            <a onclick="changeAuthTab(this)">Регистрация</a>
            <button class="modalAuth-close" onclick="closeModalAuth()">X</button>
        </div>
        <div class="modalAuth-content login">
            <form action="{{ route('login') }}" method="POST">
                @csrf
                <label for="login-email">Email</label>
                <input type="email" id="login-email" class="@error('email') is-invalid @enderror" name="email"
                       value="{{ old('email') }}"
                       required autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror
                <br>

                <label for="login-password">Пароль</label>
                <input type="password" id="login-password" class="@error('password') is-invalid @enderror"
                       name="password" required
                       autocomplete="current-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
                @enderror

                <div class="remember-checker">
                    <input class="form-check-input" type="checkbox" name="remember"
                           id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="form-check-label" for="remember">
                        {{ __('Запомнить меня') }}
                    </label>

                    @if (Route::has('password.request'))
                        <a class="btn btn-link pass-forgot" href="/password/request">
                            {{ __('Забыли пароль?') }}
                        </a>
                    @endif
                </div>

                <button type="submit">{{ __('Войти') }}</button>
            </form>
        </div>
        <div class="modalAuth-content register">
            <form action="{{ route('register') }}" method="POST">
                @csrf
                <label for="reg-name">Имя</label>
                <input id="name reg-name" type="text" class="form-control @error('name') is-invalid @enderror"
                       name="name"
                       value="{{ old('name') }}" required autocomplete="name" autofocus>
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

                <label for="reg-email">Email</label>
                <input id="email reg-email" type="email" class="form-control @error('email') is-invalid @enderror"
                       name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

                <label for="reg-password">Пароль</label>
                <input id="password reg-password" type="password"
                       class="form-control @error('password') is-invalid @enderror" name="password" required
                       autocomplete="new-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

                <label for="password-confirm">Ещё раз</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required
                       autocomplete="new-password">

                <button type="submit">{{ __('Регистрация') }}</button>
            </form>
        </div>
    </div>
</div>

<script>
    let mainPage = window.location.pathname;
    let navElem = document.getElementById('nav_btn');
    let headerElem = document.getElementById('header');

    if (mainPage === '/') {
        let catalogElem = document.getElementsByClassName('catalog');
        catalogElem[0].style.display = 'block';
    }

    navElem.onmouseover = function () {
        let navOverlay = document.getElementsByClassName('nav-overlay');

        if (navOverlay.length === 0) {
            let div = document.createElement('div');
            div.className = 'nav-overlay';
            document.body.style.position = 'relative';
            document.body.appendChild(div);
            headerElem.className = 'z-index-101';
        }
    };

    navElem.onmouseout = function () {
        let navOverlay = document.getElementsByClassName('nav-overlay');
        navOverlay[0].parentNode.removeChild(navOverlay[0]);
        headerElem.classList.remove('z-index-101');
        document.body.style.position = '';
    };
</script>

<script src="/js/slick.min.js"></script>
<script src="/js/iziModal.min.js"></script>
<script src="/js/master.js"></script>
</body>
</html>
