<div class="card card-favorites">
    <div class="card-favorites-img">
        <a href="/shop/{{ $goods_item->category }}/{{ $goods_item->subcategory }}/{{ $goods_item->id }}">
            <img src="{{ $goods_item->img_production }}" alt="">
        </a>
    </div>
    <div class="card-favorites-content">
        <div class="card-favorites-title">
            {{ $goods_item->title }}
        </div>
        <div class="card-favorites-price_and_code">
            <div class="card-favorites-price">
                <span>{{ $goods_item->price_one }}</span> грн.
            </div>
            <div class="card-favorites-code">
                {{ $goods_item->code }}
            </div>
        </div>
        <div class="card-favorites-trash">
            <a href="#" onclick="deleteGoodsFromFavorites(this, {{ $goods_item->id }}, {{ $goods_item->subcategory }})"><i class="asten_icon-delete"></i></a>
        </div>
    </div>
</div>
