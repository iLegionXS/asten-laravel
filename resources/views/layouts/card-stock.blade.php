<div class="card card-stock">
    <div class="card-stock-img">
        <a href="/shop/{{ $goods_item->category }}/{{ $goods_item->subcategory }}/{{ $goods_item->id }}">
            <img src="{{ $goods_item->img_production }}"
                 alt="{{ $goods_item->title }}">
        </a>
        <button class="card-stock-favorite"
                onclick="addFavorites({{ $goods_item->id }}, {{ $goods_item->category }}, {{ $goods_item->subcategory }})">
            <i class="asten_icon-favorite_border"></i></button>
    </div>
    <div class="card-stock-content">
        <div class="card-stock-title">
            {{ $goods_item->title }}
        </div>
        <div class="card-stock-ratings_and_comments">
            <div class="card-stock-ratings">
                <i class="asten_icon-star"></i>
                <i class="asten_icon-star"></i>
                <i class="asten_icon-star"></i>
                <i class="asten_icon-star"></i>
                <i class="asten_icon-star"></i>
            </div>
            <div class="card-stock-comments">
                <span>0 отзывов</span>
            </div>
        </div>
        <div class="card-stock-price_and_buy">
            <div class="card-stock-price">
                <span>{{ $goods_item->price_one }}  грн.</span><br>
                <span class="stock-price">
                    @foreach($stock as $item)
                        @if($item !== null && $goods_item->id === $item->goods_id)
                            {{ $item->price_fop }}
                        @endif
                    @endforeach
                    грн.</span>
            </div>
            <div class="card-stock-buy">
                <a href="/shop/{{ $goods_item->category }}/{{ $goods_item->subcategory }}/{{ $goods_item->id }}">Купить</a>
            </div>
        </div>
    </div>
</div>

