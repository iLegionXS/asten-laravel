<div class="card card-standard">
    <div class="card-standard-img">
        <a href="/shop/{{ $goods_item->category }}/{{ $goods_item->subcategory }}/{{ $goods_item->id }}">
            <img src="{{ $goods_item->img_production }}" alt="{{ $goods_item->title }}">
        </a>

        <button class="card-standard-favorite"
                onclick="addFavorites({{ $goods_item->id }}, {{ $goods_item->category }}, {{ $goods_item->subcategory }})">
            <i class="asten_icon-favorite_border"></i></button>
    </div>
    <div class="card-standard-content">
        <div class="card-standard-title">
            {{ $goods_item->title }}
        </div>
        <div class="card-standard-ratings_and_comments">
            <div class="card-standard-ratings">
                <i class="asten_icon-star"></i>
                <i class="asten_icon-star"></i>
                <i class="asten_icon-star"></i>
                <i class="asten_icon-star"></i>
                <i class="asten_icon-star"></i>
            </div>
            <div class="card-standard-comments">
                <span>0 отзывов</span>
            </div>
        </div>
        <div class="card-standard-price_and_buy">
            <div class="card-standard-price">
                <span>{{ $goods_item->price_one }}</span> грн.
            </div>
            <div class="card-standard-buy">
                    <a href="/shop/{{ $goods_item->category }}/{{ $goods_item->subcategory }}/{{ $goods_item->id }}">Купить</a>
            </div>
        </div>
    </div>
</div>

