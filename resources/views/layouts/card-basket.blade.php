<div class="card-basket">
    <div class="card-basket-img">
        <img src="{{ $goods_item->goods->img_production }}" alt="{{ $goods_item->goods->title }}">
    </div>
    <div class="card-basket-info">
        <div class="card-basket-info-title">
            {{ $goods_item->goods->title }}
        </div>
        <div class="card-basket-info-code">
            Код {{ $goods_item->goods->code }}
        </div>
        <div class="card-basket-info-price_end_count">
            <div class="price"><span>{{ $goods_item->goods->price_one }}</span> грн.</div>
            <div class="count">
                <form class="countGoods">
                    <div>
                        <button class="minus" type="button"
                                onclick="btnMinus({{ $goods_item->goods->min_count }})"><img
                                    src="/images/basket/minus.png" alt="Отнять"></button>
                        <input type="number" value="{{ $goods_item->qty }}">
                        <button class="plus" type="button" onclick="btnPlus()"><img src="/images/basket/plus.png"
                                                                                    alt="Добавить"></button>
                    </div>
                </form>
            </div>
            <div class="totalGoodsPrice"><span>{{ $goods_item->price }}</span> грн.</div>
        </div>
    </div>
    <div class="card-basket-select-color">
        <span>Выбран цвет: <button
                    class="color_{{ $goods_item->options->color }} colorGoods">{{ $goods_item->options->color }}</button></span>
        <button type="button" data-value="" onclick="openModalBasket()">Выбрать цвет</button>
    </div>
    <div class="card-basket-trash">
        <form action="/basket/del" method="post">
            @csrf
            <input type="hidden" name="del" value="{{ $goods_item->rowId }}">
            <button type="submit"><i class="asten_icon-delete"></i></button>
        </form>
    </div>
</div>

<div id="modalBasket" class="modal">
    <div class="modalWrap modalBasketWrap">
        <div class="modalHeader modalBasket-header">
            <div>Выберите цвет</div>
            <button class="modalBasket-close" onclick="closeModalBasket()">X
            </button>
        </div>
        <div class="modalContent modalBasketContent">
            <form class="color">
                <label class="color_9003">
                    9003
                    <input type="checkbox">
                </label>
                <label class="color_9005">
                    9005
                    <input type="checkbox">
                </label>
                <label class="color_3020">
                    3020
                    <input type="checkbox">
                </label>
                <label class="color_7040">
                    7040
                    <input type="checkbox">
                </label>
                <label class="color_5005">
                    5005
                    <input type="checkbox">
                </label>
                <label class="color_9006">
                    9006
                    <input type="checkbox">
                </label>
                <label class="color_7657">
                    7657
                    <input type="checkbox">
                </label>
            </form>
        </div>
    </div>
</div>
