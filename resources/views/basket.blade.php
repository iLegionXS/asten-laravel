@extends('layouts.app')

@section('content')

    <div class="breadcrumbs-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="/"><i class="asten_icon-home"></i></a> <span>/</span> Корзина
                </div>
            </div>
        </div>
    </div>

    <div class="basket-head-block head-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="basket-head head-block-head">
                        <h1>Корзина</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="basket-content-block">
        @if(isset($cart))

            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="basket-card">
                            @foreach($cart as $goods_item)
                                @include('layouts.card-basket')
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="basket-total">
                            <form action="/basket" method="GET">
                                <input type="hidden" name="vat" value="enable">
                                <button type="submit">Пересчитать с НДС</button>
                            </form>
                            <div id="basketTotal" class="basket-total-price">Всего: <span>{{ $cart->sum }}</span> грн.
                            </div>
                            <button class="basket-total-checkout" onclick="fadeToggle('.basket-form-block')">Оформить заказ</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="basket-form-block">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <form action="/basket/save" method="post">
                                @csrf
                                <div class="basket-email">
                                    <label for="email">Электронная почта</label>
                                    <input type="email" id="email" name="email">
                                </div>
                                <div class="basket-surname">
                                    <label for="surname">Фамилия</label>
                                    <input type="text" id="surname" name="surname">
                                </div>
                                <div class="basket-name">
                                    <label for="name">Имя</label>
                                    <input type="text" id="name" name="name">
                                </div>
                                <div class="basket-middlename">
                                    <label for="middlename">Отчество</label>
                                    <input type="text" id="middlename" name="middlename">
                                </div>
                                <div class="basket-phone">
                                    <label for="phone">Телефон</label>
                                    <input type="text" id="phone" name="phone">
                                </div>
                                <div class="basket-city">
                                    <label for="city">Город</label>
                                    <input type="text" id="city" name="city">
                                </div>
                                <div class="basket-delivery">
                                    <label for="delivery">Способ доставки</label>
                                    <select name="delivery" id="delivery">
                                        <option selected>Выберите один из вариантов..</option>
                                        <option value="Novaya Poshta">Новая почта</option>
                                        <option value="Delivery">Деливери</option>
                                        <option value="Intaim">Интайм</option>
                                        <option value="Avtoluks">Автолюкс</option>
                                        <option value="Samovivoz">Самовывоз</option>
                                        <option value="Poputniy">Попутный груз (при крупном объеме)</option>
                                    </select>
                                </div>
                                <div class="basket-address">
                                    <label for="address">Адрес доставки</label>
                                    <input type="text" id="address" name="address">
                                </div>

                                <button type="submit">Заказать</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @else

            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="basket-empty">
                            <h3>К сожалению, здесь пусто.</h3>
                        </div>
                    </div>
                </div>
            </div>

        @endif
    </div>

@endsection
