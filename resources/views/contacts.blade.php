@extends('layouts.app')

@section('content')

    <div class="breadcrumbs-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="/"><i class="asten_icon-home"></i></a> <span>/</span> Контакты
                </div>
            </div>
        </div>
    </div>

    <div class="contacts-head-block head-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="contacts-head head-block-head">
                        <h1>Наши <span>контакты</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contacts-content-block">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-3">
                    <div class="contacts-content-info">
                        <div>
                            <div class="address">
                                <h3>Адрес:</h3>
                                <ul>
                                    <li><i class="asten_icon-marker"></i> г. Полтава, ул. Комарова 5</li>
                                </ul>
                            </div>
                            <div class="sales_department">
                                <h3>Отдел продаж:</h3>
                                <ul>
                                    <li><i class="asten_icon-email"></i> tovvybor97@gmail.com</li>
                                    <li><i class="asten_icon-phone_in_talk"></i> +38 (099) 55-16-001</li>
                                    <li><i class="asten_icon-phone_in_talk"></i> +38 (096) 04-21-777</li>
                                    <li><i class="asten_icon-phone_in_talk"></i> +38 (063) 60-75-307</li>
                                </ul>
                            </div>
                            <div class="accounting">
                                <h3>Бухгалтерия:</h3>
                                <ul>
                                    <li><i class="asten_icon-phone_in_talk"></i> +38 (099) 551-69-11</li>
                                    <li><i class="asten_icon-phone_in_talk"></i> +38 (096) 980-01-55</li>
                                </ul>
                            </div>
                            <div class="schedule">
                                <h3>График работы:</h3>
                                <ul>
                                    <li><i class="asten_icon-clock"></i> Пн-Пт - с 9:00 до 18:00</li>
                                    <li><i class="asten_icon-clock"></i> Сб-Вс - выходные</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-9">
                    <iframe width="100%" height="466"
                            src="https://maps.google.com/maps?width=100%&amp;height=466&amp;hl=en&amp;coord=49.547645, 34.503175&amp;q=%D0%9A%D0%BE%D0%BC%D0%B0%D1%80%D0%BE%D0%B2%D0%B0%2C%205%2C%20%D0%B3.%20%D0%9F%D0%BE%D0%BB%D1%82%D0%B0%D0%B2%D0%B0+(%D0%92%D1%8B%D0%B1%D0%BE%D1%8097)&amp;ie=UTF8&amp;t=&amp;z=18&amp;iwloc=A&amp;output=embed"
                            frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                    <br/>
                </div>
            </div>
        </div>
    </div>

@endsection
