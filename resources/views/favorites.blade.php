@extends('layouts.app')

@section('content')

    <div class="breadcrumbs-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="/"><i class="asten_icon-home"></i></a> <span>/</span> Избранное
                </div>
            </div>
        </div>
    </div>

    <div class="favorites-head-block head-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="favorites-head head-block-head">
                        <h1><i class="asten_icon-favorite"></i> Избранные <span>товары</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="favorites-content-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="favorites-card"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let favorites = JSON.parse(localStorage.getItem('favorites'));

        function getFavoritesCard(id, category, subcategory) {
            let data = {
                'id': id,
                'category': category,
                'subcategory': subcategory,
            };

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "/favorites/card",
                method: 'GET',
                data: data,
                success: function (response) {
                    $('.favorites-card').append(response);
                }
            });
        }

        if (favorites !== null) {
            for (let i = 0; i < favorites.length; i++) {
                getFavoritesCard(favorites[i][0], favorites[i][1], favorites[i][2]);
            }
        } else {
            $('.favorites-card').append('Здесь пусто');
        }
    </script>

@endsection

