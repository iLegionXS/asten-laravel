@extends('layouts.app')

@section('content')

    <div class="breadcrumbs-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="/"><i class="asten_icon-home"></i></a>
                    <span>/</span>
                    <a href="/shop/{{ $category_select->id }}">{{ $category_select->title_ru }}</a>
                    <span>/</span>
                    <a href="/shop/{{ $category_select->id }}/{{ $subcategory_select->id }}">{{ $subcategory_select->title_ru }}</a>
                    <span>/</span>
                    {{ $goods_item->title }}
                </div>
            </div>
        </div>
    </div>

    <div class="details-head-block head-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="head-details-head head-block-head">
                        <a class="active" onclick="changeDetailsTabs(this)" data-value="details-main"> Главное о
                            товаре</a>
                        <a onclick="changeDetailsTabs(this)" data-value="details-specifications">Характеристики</a>
                        <a onclick="changeDetailsTabs(this)" data-value="details-reviews">Отзывы</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="details-content-block">
        <div id="details-main">
            <div class="container">
                <div class="row">
                    <div class="col-xl-2 col-lg-6 col-md-7 col-sm-8 offset-1 offset-md-2 offset-xl-0 order-xl-0 order-1">
                        <div class="img-other">
                            <div class="slick-vertical">
                                <div><img src="{{ $goods_item->img_production }}"
                                          alt="{{ $goods_item->title }}" onclick="changeImgInDetails(this.src)">
                                </div>
                                <div><img src="{{ $goods_item->img_drawing }}"
                                          alt="{{ $goods_item->title }}" onclick="changeImgInDetails(this.src)">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="img-main d-flex justify-content-center d-xl-block">
                            <img src="{{ $goods_item->img_production }}" alt="{{ $goods_item->title }}">
                        </div>
                    </div>
                    <div class="col-xl-6 order-2">
                        <div class="info">
                            <div class="head">
                                <h1>{{ $goods_item->title }}</h1>
                                <p>
                                    <span>Код товара {{ $goods_item->code }}</span>
                                    <span>Рейтинг</span>
                                    <span>Отзывы</span>
                                </p>
                            </div>
                            <div class="color">
                                <p>Цвет:</p>
                                <ul>
                                    <li>
                                        <button class="color_9003" onclick="changeColorInDetails(this)">9003</button>
                                    </li>
                                    <li>
                                        <button class="color_9005" onclick="changeColorInDetails(this)">9005</button>
                                    </li>
                                    <li>
                                        <button class="color_3020" onclick="changeColorInDetails(this)">3020</button>
                                    </li>
                                    <li>
                                        <button class="color_7040" onclick="changeColorInDetails(this)">7040</button>
                                    </li>
                                    <li>
                                        <button class="color_5005" onclick="changeColorInDetails(this)">5005</button>
                                    </li>
                                    <li>
                                        <button class="color_9006" onclick="changeColorInDetails(this)">9006</button>
                                    </li>
                                    <li>
                                        <button class="color_7657" onclick="changeColorInDetails(this)">7657</button>
                                    </li>
                                </ul>
                                <p>При необходимости другого цвета, обращайтесь к менеджеру.</p>
                            </div>
                            <div class="price">
                                <p>Цена за штуку в цвете RAL: <span id="colorGoods"><button
                                                class="color_9003">9003</button></span></p>

                                @if($stock)

                                    <p>Акционный цвет: <span id="colorGoods"><button
                                                    class="color_{{ $stock->color }}">{{ $stock->color }}</button></span></p>

                                    <table class="details-stock-table">
                                        <tr>
                                            <th>Партия, штук</th>
                                            <th colspan="2">ФОП, грн.</th>
                                            <th colspan="2">НДС, грн.</th>
                                        </tr>
                                        <tr>
                                            <td>от {{ $goods_item->min_count }}</td>
                                            <td class="del">{{ $goods_item->price_one }}</td>
                                            <td rowspan="2">{{ $stock->price_fop }}</td>
                                            <td class="del">{{ $goods_item->price_one * 1.2 }}</td>
                                            <td rowspan="2">{{ $stock->price_nds }}</td>
                                        </tr>
                                        <tr>
                                            <td>от {{ $goods_item->optimal_count }}</td>
                                            <td class="del">{{ $goods_item->price_two }}</td>
                                            <td class="del">{{ $goods_item->price_three }}</td>
                                        </tr>
                                    </table>

                                @else

                                    <table>
                                        <tr>
                                            <th>Партия, штук</th>
                                            <th>ФОП, грн.</th>
                                            <th>НДС, грн.</th>
                                        </tr>
                                        <tr>
                                            <td>от {{ $goods_item->min_count }}</td>
                                            <td>{{ $goods_item->price_one }}</td>
                                            <td>{{ $goods_item->price_one * 1.2 }}</td>
                                        </tr>
                                        <tr>
                                            <td>от {{ $goods_item->optimal_count }}</td>
                                            <td>{{ $goods_item->price_two }}</td>
                                            <td>{{ $goods_item->price_three }}</td>
                                        </tr>
                                    </table>

                                @endif
                            </div>
                            <div class="other">
                                <div class="count">
                                    <p>Количество:</p>
                                    <div id="countGoods">
                                        <div>
                                            <button class="minus" type="button"
                                                    onclick="btnMinus({{ $goods_item->min_count }})"><img
                                                        src="/images/basket/minus.png" alt="Отнять"></button>
                                            <input id="counterGoods" type="number" value="{{ $goods_item->min_count }}"
                                                   min="{{ $goods_item->min_count }}">
                                            <button class="plus" type="button" onclick="btnPlus()"><img
                                                        src="/images/basket/plus.png" alt="Добавить"></button>
                                        </div>
                                    </div>
                                </div>
                                <form id="addToCart" action="/basket/add" method="POST">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $goods_item->id }}">
                                    <input type="hidden" name="category" value="{{ $goods_item->category }}">
                                    <input type="hidden" name="subcategory" value="{{ $goods_item->subcategory }}">
                                    <input type="hidden" name="count" value="{{ $goods_item->min_count }}">
                                    <input type="hidden" name="color" value="9003">
                                    <button type="submit" class="buy">Купить</button>
                                </form>
                                <div class="favorites_and_share">
                                    <button onclick="addFavorites({{ $goods_item->id }}, {{ $category_select->id }}, {{ $subcategory_select->id }})">
                                        <i class="asten_icon-favorite_border"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="details-specifications" class="d-none">
            <div class="container">
                <div class="row">
                    <div class="col">
                        {!! $goods_item->product_characteristics !!}
                    </div>
                </div>
            </div>
        </div>
        <div id="details-reviews" class="d-none">
            <div class="container">
                <div class="row">
                    <div class="col">
                        Отзывы
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let id = {{ $goods_item->id }};
        let category = {{ $goods_item->category }};
        let subcategory = {{ $goods_item->subcategory }};
        let data = [id, category, subcategory];

        function checkEmpty() {
            return localStorage.getItem('recently-viewed');
        }

        function checkSimilar() {
            let error;
            let ls_data = JSON.parse(localStorage.getItem('recently-viewed'));

            for (let index = 0; index < 10; index++) {
                if (JSON.stringify(ls_data[index]) === JSON.stringify(data)) {
                    return error = 1;
                }
            }
        }

        function save() {
            let arr = [data];

            if (checkEmpty() === null) {
                localStorage.setItem('recently-viewed', JSON.stringify(arr))
            } else if (checkSimilar() === undefined) {
                let dataFromLS = JSON.parse(localStorage.getItem('recently-viewed'));
                dataFromLS.push(data);
                localStorage.setItem('recently-viewed', JSON.stringify(dataFromLS));
            }

            let dataFromLS = JSON.parse(localStorage.getItem('recently-viewed'));
            let count = dataFromLS.length;
            if (count > 9) {
                let mod_ls_data = dataFromLS.slice(-10);
                localStorage.setItem('recently-viewed', JSON.stringify(mod_ls_data));
            }
        }

        save();
    </script>

    <script>
        $("#counterGoods").on("change paste keyup", function () {
            $('#addToCart input[name=count]').val($(this).val());
        });

        function changeColorInDetails(elem) {
            let clone = $(elem).clone();
            let place = $('#colorGoods');

            clone.removeAttr('onclick').attr('id', 'colorSelected');
            place.find(':first-child').remove();
            place.append(clone);
            $('#addToCart input[name=color]').val(clone.text());
        }
    </script>

@endsection
