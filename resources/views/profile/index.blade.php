@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!

                        <form action="/profile/saveUserData" method="POST">
                            @csrf
                            <div class="profile-fullName">
                                <label for="fullName">Ф.И.О</label>
                                <input type="text" id="fullName" name="fullName">
                            </div>
                            <div class="profile-phone">
                                <label for="phone">Телефон</label>
                                <input type="text" id="phone" name="phone">
                            </div>
                            <div class="profile-city">
                                <label for="city">Город</label>
                                <input type="text" id="city" name="city">
                            </div>
                            <button type="submit">Сохранить</button>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
