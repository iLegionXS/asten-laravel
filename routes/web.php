<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/contacts', 'ContactsController@index');
Route::get('/payment', 'PaymentController@index');
Route::get('/shop/{category_id}/{subcategory_id?}/{goods_id?}', 'ShopController@index');
Route::get('/stock', 'StockController@index');
Route::get('/sale', 'SaleController@index');
Route::get('/favorites', 'FavoritesController@index');
Route::get('/favorites/card', 'FavoritesController@getFavorites');
Route::get('/recently_viewed', 'IndexController@recentlyViewed');
Route::get('/basket', 'BasketController@index');

Route::post('/basket/save', 'BasketController@saveItem');
Route::post('/basket/add', 'BasketController@addItem');
Route::post('/basket/del', 'BasketController@delItem');

Auth::routes();

Route::get('/profile', 'ProfileController@index');
Route::post('/profile/saveUserData', 'ProfileController@saveUserData');